import threading
import time


def thread1():
    for i in range(0, 11, 1):
        print("Thread 1: " + str(i))
        time.sleep(1)





def thread2():
    for i in range(10, -1, -1):
        print("Thread 2: " + str(i))
        time.sleep(1)


def main():
    x = threading.Thread(target=thread1, args=())
    y = threading.Thread(target=thread2, args=())
    x.start()
    y.start()
    x.join()
    y.join()


main()

print("The code is done")
